# OpenML dataset: Top-100-2020-Cryptocurrency-Daily-Market-Price

https://www.openml.org/d/43824

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Short Description
Here you are a dataset containing the top 100 coins by their total volume across all markets during 9th January of 2021. The prices are in USD dollars.
If you need a different or specific cryptocurrency data, open a new Discussion and I will try to do my best.
Content
This dataset was obtained thanks to cryptocompare API. You can see that it contains data about the top 100 coins by volume market during moreover 2020 (it contains a little bit of 2019 and 2021 years also). The dataset has 10 columns:

datetime: the date in which the coin had that price and volume.
low: the lowwst price of that day in USD dollars.
high: the highest price of that day in USD dollars.
open: the price when that day started in the markets in USD dollars.
close: the price when that day closed in the markets in USD dollars.
volumefrom: the quantity of that coin that was traded in that day.
volumeto: the quantity of that coin that was traded in that day in USD dollars.
cryptocurrency: the symbol of the cryptocurrency.
image_url: the image url containing the cryptocurrency logo.
coin_name: the full name of the cryptocurrency.

To get the data I used this endpoint and to get the top 100 coins list I used this one.
The code I used to generate can be found in my personal github (I would really appreciate any contribution, follow or star ). Feel free to contact me (you can find my email in my github) if you need something about the data.
Acknowledgements
Part of the code used to obtain the data was inspired in this fabulous analsys from Roman Orac.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43824) of an [OpenML dataset](https://www.openml.org/d/43824). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43824/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43824/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43824/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

